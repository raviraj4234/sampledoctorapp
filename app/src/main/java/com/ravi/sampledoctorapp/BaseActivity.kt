package com.ravi.sampledoctorapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ravi.sampledoctorapp.R

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())
        initViews()
    }

     abstract fun getLayoutResId():Int
     abstract fun initViews()

}
