

package com.ravi.sampledoctorapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ravi.sampledoctorapp.R


abstract class BaseFragment : Fragment() {
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        return inflater.inflate(getLayoutResId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPresenter()
        initViews(view, savedInstanceState)
    }

    abstract fun initPresenter()

    abstract fun initViews(view: View, savedInstanceState: Bundle?)

    abstract fun getLayoutResId():Int

    abstract fun onBackPressed()
}
