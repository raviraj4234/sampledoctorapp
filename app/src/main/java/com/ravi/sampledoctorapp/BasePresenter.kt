

package com.ravi.sampledoctorapp

interface BasePresenter {
    fun start()
    fun stop()
}