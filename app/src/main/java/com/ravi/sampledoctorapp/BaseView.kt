
package com.ravi.sampledoctorapp

interface BaseView<T> {

    var presenter: T
    var isActive: Boolean

    fun setLoadingIndicator(active: Boolean)
}
