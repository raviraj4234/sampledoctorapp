package com.ravi.sampledoctorapp.helpers

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager


/**
 * This provides methods to help Activities load their UI.
 */
object ActivityUtils {

    fun addFragmentToActivity(fragmentManager: FragmentManager,
                              fragment: Fragment, frameId: Int, addToBackstack: Boolean) {
        val transaction = fragmentManager.beginTransaction()

        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
        transaction.add(frameId, fragment, fragment.javaClass.name)
        if (addToBackstack)
            transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }

    fun replaceFragmentToActivity(fragmentManager: FragmentManager,
                                  fragment: Fragment, frameId: Int, addToBackstack: Boolean) {
        val transaction = fragmentManager.beginTransaction()

        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
        transaction.replace(frameId, fragment, fragment.javaClass.name)
        if (addToBackstack)
            transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }


}
