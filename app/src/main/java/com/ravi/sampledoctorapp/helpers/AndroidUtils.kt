package com.ravi.sampledoctorapp.helpers

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.text.SimpleDateFormat
import java.util.*

object AndroidUtils {

    fun parseMessageTimestamp(timestamp: Long): String {
        try {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp
            val sdf = SimpleDateFormat("HH:mm")
            val currenTimeZone = calendar.time as Date
            return sdf.format(currenTimeZone)
        } catch (e: Exception) {
        }

        return ""
    }

    fun hideKeyboard(activity: Activity?) {
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity!!.currentFocus
        if (null == view)
            view = View(activity)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}