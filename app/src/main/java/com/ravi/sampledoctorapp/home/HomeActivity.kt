package com.ravi.sampledoctorapp.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.ActionBar
import com.ravi.sampledoctorapp.BaseActivity
import com.ravi.sampledoctorapp.BaseFragment
import com.ravi.sampledoctorapp.R
import com.ravi.sampledoctorapp.helpers.ActivityUtils
import com.ravi.sampledoctorapp.home.chatlist.ChatListFragment
import com.ravi.sampledoctorapp.home.doctorslist.DoctorsListFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {

    //    private lateinit var mActionBar: ActionBar
    override fun getLayoutResId(): Int {
        return R.layout.activity_home
    }

    override fun initViews() {
        setSupportActionBar(toolbar_home)
//        mActionBar = supportActionBar!!
//        mActionBar.setDisplayHomeAsUpEnabled(false)
//        mActionBar.setDisplayShowHomeEnabled(false)

        ActivityUtils.replaceFragmentToActivity(supportFragmentManager, DoctorsListFragment.newInstance(), R.id.FL_container_body, false)
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {

        val fragment = supportFragmentManager.findFragmentById(R.id.FL_container_body)
        (fragment as? BaseFragment)?.onBackPressed()

        super.onBackPressed()
    }

}
