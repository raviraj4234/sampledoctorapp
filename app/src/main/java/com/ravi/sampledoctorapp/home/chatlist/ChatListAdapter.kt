package com.ravi.sampledoctorapp.home.chatlist

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatImageView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ravi.sampledoctorapp.R
import android.widget.TextView
import android.support.v7.widget.RecyclerView
import android.widget.RatingBar
import com.ravi.sampledoctorapp.home.chatlist.model.ChatItemsModel
import android.R.attr.button
import android.view.Gravity
import android.R.attr.gravity
import android.support.v4.content.ContextCompat
import android.view.ViewGroup.LayoutParams.FILL_PARENT
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.ravi.sampledoctorapp.helpers.AndroidUtils


class ChatListAdapter(private val chatList: MutableList<ChatItemsModel>) : RecyclerView.Adapter<ChatListAdapter.ChatListHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_chat_list_item, parent, false)

        return ChatListHolder(itemView)
    }

    override fun getItemCount(): Int {
        return chatList.size
    }

    override fun onBindViewHolder(holder: ChatListHolder, position: Int) {
        val params = holder.clContainer.layoutParams as FrameLayout.LayoutParams
        if (chatList[position].isMyMsg) {
            params.gravity = Gravity.END
            params.marginStart = holder.itemView.context.resources.getDimensionPixelOffset(R.dimen._50sdp)
            holder.clContainer.setLayoutParams(params)
        } else {
            params.gravity = Gravity.START
            params.marginEnd = holder.itemView.context.resources.getDimensionPixelOffset(R.dimen._50sdp)
            holder.clContainer.setLayoutParams(params)
        }

        holder.tvMsg.text = chatList[position].message
        holder.tvTime.text = AndroidUtils.parseMessageTimestamp(chatList[position].time)
    }

    inner class ChatListHolder(view: View) : RecyclerView.ViewHolder(view) {
        val clContainer: ConstraintLayout = view.findViewById(R.id.CL_chat_list_item_container)
        val tvMsg: TextView = view.findViewById(R.id.TV_chat_list_item_msg)
        val tvTime: TextView = view.findViewById(R.id.TV_chat_list_item_time)

    }

    fun addMessage(chatItemsModel: ChatItemsModel) {
        chatList.add(chatItemsModel)
        notifyItemInserted(chatList.size - 1)
//        notifyDataSetChanged()
    }
}
