package com.ravi.sampledoctorapp.home.chatlist

import com.ravi.sampledoctorapp.BasePresenter
import com.ravi.sampledoctorapp.BaseView
import com.ravi.sampledoctorapp.home.chatlist.model.ChatItemsModel


interface ChatListContract {

    interface View : BaseView<Presenter> {


    }

    interface Presenter : BasePresenter {
        var chatList: MutableList<ChatItemsModel>
        fun createMessage(msg: String): ChatItemsModel
    }
}
