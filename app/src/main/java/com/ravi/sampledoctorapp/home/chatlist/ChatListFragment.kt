package com.ravi.sampledoctorapp.home.chatlist

import android.app.Activity
import android.inputmethodservice.InputMethodService
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.ravi.sampledoctorapp.BaseFragment

import com.ravi.sampledoctorapp.R
import com.ravi.sampledoctorapp.helpers.ActivityUtils
import com.ravi.sampledoctorapp.helpers.AndroidUtils
import com.ravi.sampledoctorapp.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_chat.*
import org.jetbrains.anko.selector


class ChatListFragment : BaseFragment(), ChatListContract.View {
    override fun onBackPressed() {
        val mActionBar = (activity as HomeActivity).supportActionBar
        mActionBar?.title = "Doctors"
        mActionBar?.setDisplayHomeAsUpEnabled(false)
        mActionBar?.setDisplayShowHomeEnabled(false)
        AndroidUtils.hideKeyboard(activity)
    }


    private var mPresenter: ChatListContract.Presenter? = null
    private var chatlistAdapter: ChatListAdapter? = null

    override fun initPresenter() {
        ChatListPresenter(this)
    }

    override fun initViews(view: View, savedInstanceState: Bundle?) {

        val name = arguments?.getString("name")
        val mActionBar = (activity as HomeActivity).supportActionBar
        mActionBar?.title = name
        mActionBar?.setDisplayHomeAsUpEnabled(true)
        mActionBar?.setDisplayShowHomeEnabled(true)

        RV_chatlist.layoutManager = LinearLayoutManager(context)
        chatlistAdapter = ChatListAdapter(mPresenter!!.chatList)
        RV_chatlist.setHasFixedSize(true)
        RV_chatlist.adapter = chatlistAdapter

        IV_send_msg.setOnClickListener {
            if (ET_msg.text.toString().trim().isNotBlank()) {
                chatlistAdapter!!.addMessage(mPresenter!!.createMessage(ET_msg.text.toString().trim()))
                ET_msg.setText("")
                RV_chatlist.scrollToPosition(chatlistAdapter!!.itemCount - 1)
            }
        }
        IV_attachment.setOnClickListener {
            AndroidUtils.hideKeyboard(activity)
            val options = listOf("Camera", "Gallery", "Documents")
            activity?.selector(
                    "Choose Option", options) { dialogInterface, i ->
            }
        }
    }

    override fun getLayoutResId(): Int {
        return R.layout.fragment_chat
    }

    override var isActive = isAdded


    override var presenter: ChatListContract.Presenter
        get() = mPresenter!!
        set(presenter) {
            mPresenter = presenter
        }


    override fun setLoadingIndicator(active: Boolean) {

    }

    companion object {

        fun newInstance(name: String): ChatListFragment {
            val bundle = Bundle()
            bundle.putString("name", name)
            val fragment = ChatListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
