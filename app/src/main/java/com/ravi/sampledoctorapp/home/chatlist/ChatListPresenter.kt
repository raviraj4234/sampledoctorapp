package com.ravi.sampledoctorapp.home.chatlist

import android.app.Activity
import com.ravi.sampledoctorapp.home.chatlist.model.ChatItemsModel


class ChatListPresenter(private val mView: ChatListContract.View) : ChatListContract.Presenter {
    override fun stop() {

    }

    init {
        mView.presenter = this

    }

    override fun start() {

    }

    override var chatList: MutableList<ChatItemsModel> =  ArrayList()


    override fun createMessage(msg: String): ChatItemsModel {
        val chatItemsModel = ChatItemsModel()
        chatItemsModel.chatId = System.currentTimeMillis().toString()
        chatItemsModel.isMyMsg = true
        chatItemsModel.message = msg
        chatItemsModel.time = System.currentTimeMillis()
        return chatItemsModel
    }


}
