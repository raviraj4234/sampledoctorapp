package com.ravi.sampledoctorapp.home.doctorslist

import com.ravi.sampledoctorapp.home.doctorslist.model.Datum

interface DoctorListItemListener {
    fun onItemClicked(position: Int,datum: Datum)
}