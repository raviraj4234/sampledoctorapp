package com.ravi.sampledoctorapp.home.doctorslist

import android.support.v7.widget.AppCompatImageView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ravi.sampledoctorapp.R
import android.widget.TextView
import android.support.v7.widget.RecyclerView
import android.widget.RatingBar
import com.ravi.sampledoctorapp.home.doctorslist.model.Datum
import com.ravi.sampledoctorapp.home.doctorslist.model.Link
import com.squareup.picasso.Picasso
import java.lang.Exception


class DoctorsListAdapter(private val doctorsList: MutableList<Datum>, private val doctorListItemListener: DoctorListItemListener) : RecyclerView.Adapter<DoctorsListAdapter.DoctorsListHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoctorsListHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_doctorslist_item, parent, false)

        return DoctorsListHolder(itemView)
    }

    override fun getItemCount(): Int {
        return doctorsList.size
    }

    override fun onBindViewHolder(holder: DoctorsListHolder, position: Int) {
        holder.tvName.text = doctorsList[position].firstName + " " + doctorsList[position].lastName
        if (null != doctorsList[position].rating)
            holder.ratingBar.rating = doctorsList[position].rating.toFloat()
        else holder.ratingBar.rating = 0f
        Picasso.get().load(getProfilePictureLink(doctorsList[position].links)).placeholder(R.drawable.ic_profile_no_image)
                .error(R.drawable.ic_profile_no_image).into(holder.ivProfile)
        holder.rlParentView.setOnClickListener {
            doctorListItemListener.onItemClicked(position, doctorsList[position])
        }

    }

    inner class DoctorsListHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.TV_doctorslist_item_name)
        var ivProfile: AppCompatImageView = view.findViewById(R.id.IV_doctorslist_item_profile)
        var ratingBar: RatingBar = view.findViewById(R.id.RT_doctorslist_item_rating)
        val rlParentView: View = view.findViewById(R.id.RL_doctorlist_item_parent_container)

    }

    fun getProfilePictureLink(links: MutableList<Link>): String? {

        return try {
            links.single {
                it.rel == "profile_photo"
            }.href
        } catch (exception: Exception) {
            null
        }

    }


    fun addMoreData(doctorsList: MutableList<Datum>) {
        val earlierPos = doctorsList.size
        this.doctorsList.addAll(doctorsList)
        notifyItemRangeChanged(earlierPos, doctorsList.size - 1)
    }

}
