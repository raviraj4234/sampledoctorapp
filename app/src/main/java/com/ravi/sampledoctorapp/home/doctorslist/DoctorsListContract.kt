package com.ravi.sampledoctorapp.home.doctorslist

import com.ravi.sampledoctorapp.BasePresenter
import com.ravi.sampledoctorapp.BaseView
import com.ravi.sampledoctorapp.home.doctorslist.model.DoctorDataModel
import com.ravi.sampledoctorapp.home.doctorslist.model.Meta


interface DoctorsListContract {

    interface View : BaseView<Presenter> {

        fun showDoctorsList(doctorDataModel: DoctorDataModel)
        fun showErrorMsg(msg: String)
        fun showNetworkErrorMsg()
        fun isNetworkAvailable(): Boolean

    }

    interface Presenter : BasePresenter {
        fun getMeta(): Meta
        fun fetchDoctorsList(pageNo: Int)


    }
}
