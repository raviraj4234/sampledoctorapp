package com.ravi.sampledoctorapp.home.doctorslist

import android.app.Dialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ProgressBar
import com.ravi.sampledoctorapp.BaseFragment

import com.ravi.sampledoctorapp.R
import com.ravi.sampledoctorapp.helpers.ActivityUtils
import com.ravi.sampledoctorapp.helpers.NetworkUtils
import com.ravi.sampledoctorapp.helpers.PaginationScrollListener
import com.ravi.sampledoctorapp.home.HomeActivity
import com.ravi.sampledoctorapp.home.chatlist.ChatListFragment
import com.ravi.sampledoctorapp.home.doctorslist.model.Datum
import com.ravi.sampledoctorapp.home.doctorslist.model.DoctorDataModel
import com.ravi.sampledoctorapp.network.ApiService
import com.ravi.sampledoctorapp.network.ApiClient
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_doctorslist.*
import org.jetbrains.anko.design.snackbar


class DoctorsListFragment : BaseFragment(), DoctorsListContract.View {

    private var progressBar: ProgressBar? = null
    private var doctorsListAdapter: DoctorsListAdapter? = null
    private var dialog: Dialog? = null

    val PAGE_START = 1
    private var isLoading = false
    private var isLastpage = false
    private var totalPages = PAGE_START
    private var currentPage = PAGE_START


    override fun showDoctorsList(doctorDataModel: DoctorDataModel) {
        totalPages = Math.ceil((mPresenter?.getMeta()!!.total * 1.0) / (mPresenter?.getMeta()!!.pageSize)).toInt()
        setAdapter(doctorDataModel.data)
        ++currentPage
        isLoading = false
        if (currentPage > totalPages)
            isLastpage = true
    }

    override fun onBackPressed() {
        mPresenter?.stop()
    }

    private fun setAdapter(doctorsList: MutableList<Datum>) {
        if (null == doctorsListAdapter) {
            doctorsListAdapter = DoctorsListAdapter(doctorsList, object : DoctorListItemListener {
                override fun onItemClicked(position: Int, datum: Datum) {
                    ActivityUtils.addFragmentToActivity(activity!!.supportFragmentManager, ChatListFragment.newInstance(datum.firstName + " " + datum.lastName), R.id.FL_container_body, true)
                }
            })
            RV_doctorslist.adapter = doctorsListAdapter
        } else {
            doctorsListAdapter?.addMoreData(doctorsList)
        }
    }

    override fun showErrorMsg(msg: String) {
        isLoading = false
        if (msg.isNotBlank())
            (activity as HomeActivity).CL_home_snackbar_container.snackbar(msg)
        else (activity as HomeActivity).CL_home_snackbar_container.snackbar(R.string.msg_something_went_wrong)

    }


    override fun showNetworkErrorMsg() {
        isLoading = false
        if (1 == currentPage)
            (activity as HomeActivity).CL_home_snackbar_container.snackbar(R.string.msg_check_network_connection)

    }

    override fun isNetworkAvailable(): Boolean {
        return NetworkUtils.isConnected(context)
    }


    private var mPresenter: DoctorsListContract.Presenter? = null


    override fun initPresenter() {
        DoctorsListPresenter(this, ApiClient.getClient(context)
                .create(ApiService::class.java))
    }

    override fun initViews(view: View, savedInstanceState: Bundle?) {
        val mActionBar = (activity as HomeActivity).supportActionBar
        mActionBar?.title = "Doctors"
        mActionBar?.setDisplayHomeAsUpEnabled(false)
        mActionBar?.setDisplayShowHomeEnabled(false)
        RV_doctorslist.layoutManager = LinearLayoutManager(context)

        mPresenter?.fetchDoctorsList(currentPage)

        RV_doctorslist.addOnScrollListener(object : PaginationScrollListener(RV_doctorslist.layoutManager as LinearLayoutManager) {
            override fun totalPageCount(): Int {
                return totalPages
            }

            override fun isLastPage(): Boolean {
                return isLastpage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {

                isLoading = true

                mPresenter?.fetchDoctorsList(currentPage)

            }

        })


    }

    override fun getLayoutResId(): Int {
        return R.layout.fragment_doctorslist
    }


    override var isActive: Boolean
        get() = isAdded
        set(value) {}

    override var presenter: DoctorsListContract.Presenter
        get() = mPresenter!!
        set(presenter) {
            mPresenter = presenter
        }


    override fun setLoadingIndicator(active: Boolean) {
        /*if (null != dialog) {
            if (active)
                dialog?.show()
            else dialog?.hide()
        } else {
            dialog = Dialog(context)
            dialog?.setCancelable(false)
            dialog?.setCanceledOnTouchOutside(false)
            dialog?.show()
        }*/
        if (active)
            PB_doctorslist.visibility = View.VISIBLE
        else
            PB_doctorslist.visibility = View.GONE
    }

    fun loadFirstPage() {


    }

    fun loadNextPage() {
        isLoading = false

        if (currentPage == totalPages)
            isLastpage = true

    }


    companion object {

        fun newInstance(): DoctorsListFragment {
            return DoctorsListFragment()
        }
    }
}
