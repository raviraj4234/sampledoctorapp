package com.ravi.sampledoctorapp.home.doctorslist

import com.ravi.sampledoctorapp.network.ApiService
import com.ravi.sampledoctorapp.home.doctorslist.model.DoctorDataModel
import com.ravi.sampledoctorapp.home.doctorslist.model.Meta
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class DoctorsListPresenter(private val mView: DoctorsListContract.View, private val apiService: ApiService) : DoctorsListContract.Presenter {
    override fun getMeta(): Meta {
        return meta!!
    }

    private var meta: Meta? = null
    private val disposable: CompositeDisposable = CompositeDisposable()

    init {

        mView.presenter = this
    }

    override fun start() {

    }

    override fun stop() {
        mView.setLoadingIndicator(false)
        disposable.clear()
    }


    override fun fetchDoctorsList(pageNo: Int) {
        if (!mView.isNetworkAvailable()) {
            mView.showNetworkErrorMsg()
            return
        }
        mView.setLoadingIndicator(true)
        disposable.add(apiService.fetchDoctorsList(10, pageNo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<DoctorDataModel>() {
                    override fun onSuccess(doctorDataModel: DoctorDataModel) {
                        if (mView.isActive) {
                            mView.setLoadingIndicator(false)
                            if (null != doctorDataModel.meta && null != doctorDataModel.data) {
                                meta = doctorDataModel.meta
                                mView.showDoctorsList(doctorDataModel)
                            } else {
                                mView.showErrorMsg("")
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (mView.isActive) {
                            mView.setLoadingIndicator(false)
                            if (mView.isNetworkAvailable())
                                mView.showErrorMsg("")
                            else mView.showNetworkErrorMsg()

                        }
                    }
                }))

    }
}
