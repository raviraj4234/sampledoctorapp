
package com.ravi.sampledoctorapp.home.doctorslist.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorDataModel implements Parcelable
{

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    public final static Creator<DoctorDataModel> CREATOR = new Creator<DoctorDataModel>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DoctorDataModel createFromParcel(Parcel in) {
            return new DoctorDataModel(in);
        }

        public DoctorDataModel[] newArray(int size) {
            return (new DoctorDataModel[size]);
        }

    }
    ;

    protected DoctorDataModel(Parcel in) {
        this.meta = ((Meta) in.readValue((Meta.class.getClassLoader())));
        in.readList(this.data, (Datum.class.getClassLoader()));
    }

    public DoctorDataModel() {
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(meta);
        dest.writeList(data);
    }

    public int describeContents() {
        return  0;
    }

}
