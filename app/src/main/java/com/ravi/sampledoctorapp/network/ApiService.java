package com.ravi.sampledoctorapp.network;

import com.ravi.sampledoctorapp.home.doctorslist.model.DoctorDataModel;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("public/search/physicians?expand=qualifications,reviews_count,specializations")
    Single<DoctorDataModel> fetchDoctorsList(@Query("page_size") int pagesize, @Query("page_no") int pageNo);
}
